package br.com.javaparaweb.financeiro.usuario;

import java.util.List;

import br.com.javaparaweb.util.DaoFactory;

public class UsuarioRN {
	private UsuarioDao usuarioDao;

	public UsuarioRN() {
		this.usuarioDao = DaoFactory.criarUsuarioDao();
	}

	public void salvar(Usuario usuario) {

		if (usuario.getUsuarioId() != null) {
			usuarioDao.atualizar(usuario);
		} else {
			usuarioDao.salver(usuario);
		}
	}

	public void delete(Integer usuarioId) {
		usuarioDao.remover(usuarioId);
	}

	public List<Usuario> findAll() {
		return usuarioDao.FindAll();

	}

	public Usuario findLogin(String login) {
		return usuarioDao.buscaPorLogin(login);

	}
	
	public  Usuario findByPrimaryKey(Integer usuarioId) {
		return usuarioDao.findByPrimaryKey(usuarioId);
	}

}
