package br.com.javaparaweb.financeiro.usuario;

import java.util.List;

public interface UsuarioDao {

	
	public void salver(Usuario usuario);
	public void atualizar(Usuario usuario);
	public void remover(Integer usuarioId);
	public List<Usuario> FindAll();
	public Usuario buscaPorLogin(String login);
	public Usuario findByPrimaryKey(Integer usuarioId);
}
