package br.com.javaparaweb.financeiro.usuario;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class UsuarioDaoHibernate implements UsuarioDao {
	private Session session;
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salver(Usuario usuario) {
		session.save(usuario);
		
	}

	@Override
	public void atualizar(Usuario usuario) {
		session.update(usuario);
		
	}

	@Override
	public void remover(Integer usuarioId) {
	  Usuario usuario =  findByPrimaryKey(usuarioId);
		session.delete(usuario);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> FindAll() {
	
		return session.createCriteria(Usuario.class).list();
	}

	@Override
	public Usuario buscaPorLogin(String login) {
		Query query = session.createQuery("Select u From "+Usuario.NAME+" u Where u.login  = :login");
		query.setParameter("login",login);
		return (Usuario) query.uniqueResult();
	}

	@Override
	public Usuario findByPrimaryKey(Integer usuarioId) {
	 
		return  (Usuario) session.get(Usuario.class,usuarioId);
	}

}
