package br.com.javaparaweb.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import br.com.javaparaweb.util.HibernateUtil;

@WebFilter(urlPatterns = { "*.jsf" })
public class ConexaoHibernateFilter implements Filter {
	private SessionFactory sessionFactory;

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		Transaction transaction = null;
		Session session = null;
		try {
			session = this.sessionFactory.openSession();

			transaction = session.getTransaction();
			transaction.begin();
			arg2.doFilter(arg0, arg1);

			transaction.commit();

			if (transaction.isActive()) {
				session.close();
			}

		} catch (Exception e) {
			System.out.println("Erro na Classe de Filtro ");
			e.printStackTrace();
			if (transaction.isActive()) {
				transaction.rollback();
				session.close();
			}
		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("========================Servidor Inicializado=======================");
		sessionFactory = HibernateUtil.getSessionFactory();

	}

}
