package br.com.javaparaweb.web.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.javaparaweb.financeiro.usuario.Usuario;

@ManagedBean
@ViewScoped
public class UsuarioBean {

	private  Usuario usuario;
	private String confirmaSenha;
	
	@PostConstruct
	public void Init() {
		usuario =  new Usuario();
		confirmaSenha = "";
	}

	public Usuario getUsuario() {
		return usuario;
	}
	

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getConfirmaSenha() {
		return confirmaSenha;
	}

	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}
	
	
	
	
	
}
