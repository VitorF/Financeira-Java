package br.com.javaparaweb.util;

import org.hibernate.Session;

import br.com.javaparaweb.financeiro.usuario.UsuarioDao;
import br.com.javaparaweb.financeiro.usuario.UsuarioDaoHibernate;

public class DaoFactory {
	

	public static UsuarioDao criarUsuarioDao() {
		UsuarioDaoHibernate usuarioDao =  new UsuarioDaoHibernate();
		Session session =  HibernateUtil.getSessionFactory().openSession();
		usuarioDao.setSession(session);
		
		return usuarioDao;
	}
	
	
	
	
	
}
