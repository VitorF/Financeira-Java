package br.com.javaparaweb.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static SessionFactory sessionFactory = null;

	static {
		try {

			if (sessionFactory == null) {
				Configuration cof = new Configuration();
				cof.configure("hibernate.cfg.xml");
				StandardServiceRegistryBuilder registerBuilder = new StandardServiceRegistryBuilder();
				registerBuilder.applySettings(cof.getProperties());
				StandardServiceRegistry serviceRegistry = registerBuilder.build();
				sessionFactory = cof.buildSessionFactory(serviceRegistry);
			}

		} catch (Exception e) {
			System.out.println("erro ao conectar com o banco ");
			e.printStackTrace();
		}

	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
